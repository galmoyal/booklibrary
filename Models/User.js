// export enum UserType {
//     Client,
//     Admin
// }

export default class User {
    _id;          // String;
    _name;       // String;
    _password;   // string;
    _type;       // string - UserType;
    // TODO ctor has no validation
    constructor(id, name, password) {
        this._id = id;
        this._name = name;
        this._password = password
    }

    get Id() {
        return this._id;
    }

    get UserName() {
        return this._name;
    }

    get Password() {
        return this._password;
    }

    get Type() {
        return this._type
    }

    set Id(newId) {
        this._id = newId;
    }

    set UserName(newName) {
        if (this.validateName(newName)) {
            this._name = newName;
        }
    }

    set Password(newPass) {
        if (this.validatePass(newPass)) {
            this._password = newPass;
        }
    }

    set Type(newType) {
        if (newType == "Client" || newType == "Admin") {
            this._type = newType;
        }
        else {
            console.log('try to set type failed - unsupported value provided: ' + newType);
        }
    }

    validateName(val) {
        if ((!val.length) && val.length >= 3 && val.length <= 10) {
            return true;
        }
        else {
            console.log("galmsg(should ret to cli) - Please enter a valid username: between 3 to 10 valid (word) charcaters!")
            return false;
        }
    }

    validatePass(val) {
        if ((!val.length) && val.length >= 5 && val.length <= 15) {
            return true;
        }
        else {
            console.log("galmsg(should ret to cli) - Password must consist of 5 to 15 charcters!")
            return false;
        }
    }
}
