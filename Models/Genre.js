class Genre {
    Id; // string
    _Name; // string
    Books; // Book[]

    constructor(Id, Name, Books) {
        this.Id = Id
        this._Name = Name;
        this.Books = Books
    }
    get Name() {
        return this._Name;
    }
    set Name(newName) {
        if (this.validateName(newName)) {
            this._Name = newName;
        }
    }
    validateName(val) {
        if ((!val.length) && val.length >= 3 && val.length <= 20) {
            // TODO check that contains only english characters and digits
            return true;
        }
        else {
            console.log("galmsg(should ret to cli) - Please enter a valid username: between 3 to 15 valid (word) charcaters!")
            return false;
        }
    }
}