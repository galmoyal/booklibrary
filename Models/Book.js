class Book {
    Id; // string
    _BookName; // string
    _Author; // string
    ReleaseDate; // date
    _Description; // string
    ImageId; // string
    Image; // IImage
    Genres; // []IGenre
    PublisherId; // string
    Publisher; // IPublisher

    // TODO ctor has no validation
    constructor(id, bookName, author, releaseDate, description, imageId, image, genres, publisherId, publisher) {
        this.Id = id;
        this._BookName = bookName;
        this._Author = author;
        this.ReleaseDate = releaseDate;
        this._Description = description;
        this.ImageId = imageId;
        this.Image = image;
        this._Genres = genres;
        this._PublisherId = publisherId;
        this._Publisher = publisher;
    }
    get BookName() {
        return this._BookName;
    }
    set BookName(newName) {
        if (this.validateName(newName)) {
            this._BookName = newName;
        }
    }
    get Author() {
        return this._Author;
    }
    set Author(newAuthor) {
        if (this.validateName(newAuthor)) {
            this._Author = newAuthor;
        }
    }
    get Description() {
        return this._Description;
    }
    set Description(newDesc) {
        if (this.validateName(newDesc)) {
            this._Description = newDesc;
        }
    }
    validateName(val) {
        if ((!val.length) && val.length >= 3 && val.length <= 15) {
            return true;
        }
        else {
            console.log("galmsg(should ret to cli) - Please enter a valid username: between 3 to 15 valid (word) charcaters!")
            return false;
        }
    }
}