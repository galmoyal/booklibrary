// POST: <serverUrl>/Book/
export let createBook = {
    imageId,// String
    publisherId,// String
    GenresIds// String
}

// GETMANY response object: <serverUrl>/Book/
export let bookRO = {
    image,// ImageRo,
    publisher,// String,
    Genres// Genere[]
}

// GET:
export let updateBook = {
    newGenres// Array<IGenre>

}

export let IBook = {
    coverImage,// ICoverImage;
    publisher,// IPublisher;
    Genres// IGenre[]
}

app.get('/Books/Watch', function (req, res) {
    // return all books, generes populated
})

app.get('/Books', function (req, res) {
    // return all books
})

app.get('/Books/Details/<TODO-ID>', function (req, res) {
    // get bookbyid?
})

app.post('/Books/create', function (req, res) {
    /**
     * // POST: Books/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,BookName,Author,ReleaseDate,Description,ImageId,PublisherId")] Book book, int[] genresIds)
        {
            if (ModelState.IsValid)
            {
                String newBookName = book.BookName.ToLower().Trim();
                if (blueLibraryConext.Book.FirstOrDefault(b =>
                    b.BookName.ToLower().Trim().Equals(newBookName)) != null)
                {
                    ViewData["ImageId"] = new SelectList(
                        blueLibraryConext.BookImage.Include(b => b.Book).Where(b => b.Book == null), "Id", "ImageURL");
                    ViewData["PublisherId"] = new SelectList(blueLibraryConext.Publisher, "Id", "Name");
                    ViewData["GenresIds"] = new MultiSelectList(blueLibraryConext.Genre, "Id", "Name");
                    ViewData["Error"] = "Book with exact same name already exists";
                    return View(book);
                }

                book.Genres = new List<Genre>();
                foreach (var genreId in genresIds)
                {
                    book.Genres.Add(blueLibraryConext.Genre.FirstOrDefault(g => g.Id == genreId));
                }
                blueLibraryConext.Add(book);
                await blueLibraryConext.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["ImageId"] = new SelectList(blueLibraryConext.BookImage, "Id", "Id", book.ImageId);
            ViewData["PublisherId"] = new SelectList(blueLibraryConext.Publisher, "Id", "Id", book.PublisherId);
            return View(book);
        }
     */
})

app.post('/Books/Edit/<TODO-ID>', function (req, res) {
    // admin access
    // edit by id 
})

app.post('/Books/Delete/<TODO-ID>', function (req, res) {
    // admin access
    // delete by id 
})

// post to tweeter magic? 
/**
 * 
 * [HttpPost]
        public async Task<IActionResult> PostOnTwitter(string bookName, string Image, string tweets)
        {
            string key = "G8eiLF0oqm66BMWSCdxvQOxpD";
            string secret = "X9jtSMahKw8hPe4b5Xj6ds39DKOLX9ufXaiEVDFvkCmVgKkfBS";
            string token = "1431627254837260290-1ESplU5nuBxZKTOrWbqUgMNfAwC76H";
            string tokenSecret = "CFm5gdzzKL0e0Ak1TkukVIMTkg4hThg6eklX3t0RU8JKN";


            var service = new TweetSharp.TwitterService(key, secret);
            service.AuthenticateWith(token, tokenSecret);

            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData("wwwroot/" + Image);

            if (bytes != null)
            {
                using (var stream = new MemoryStream(bytes))
                {
                    var tweetToPost = new SendTweetWithMediaOptions
                    {
                        Status = tweets,
                        Images = new Dictionary<string, Stream> { { "myPic", stream } }
                    };
                    var result = service.SendTweetWithMedia(tweetToPost);
                    if (result == null)
                    {
                        ViewData["Error"] = "Cannot post Tweet";
                    }
                }
            }
            else
            {
                var tweetToPost = new SendTweetOptions
                {
                    Status = tweets
                };
                var result = service.SendTweet(tweetToPost);
                if (result == null)
                {
                    ViewData["Error"] = "Cannot post Tweet";
                }
            }
            return RedirectToAction(nameof(Watch));
        }
 */

app.get('/Books/ByAuthor', function (req, res) {
    // return books by author
})